from django.shortcuts import render


def contact(request):
    return render(request, 'contact.html')


def order(request):
    return render(request, 'order.html')


def mainpage(request):
    return render(request, 'mainpage.html')


def product(request):
    return render(request, 'product.html')


def description(request):
    return render(request, 'description.html')

