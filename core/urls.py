from django.urls import path

from core.views import contact, order, mainpage, product, description

urlpatterns = [
    path('', mainpage, name="mainpage"),
    path('contact/', contact, name="contact"),
    path('order/', order, name="order"),
    path('mainpage/', mainpage, name="mainpage"),
    path('product/', product, name="product"),
    path('description/', description, name="description"),

]
